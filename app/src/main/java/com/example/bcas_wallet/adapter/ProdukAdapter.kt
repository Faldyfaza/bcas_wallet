package com.example.bcas_wallet.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bcas_wallet.databinding.ItemProdukBinding
import com.example.bcas_wallet.databinding.ItemPromoBinding
import com.example.bcas_wallet.model.ProdukModel


class ProdukAdapter(
    private val dataProduk: List<ProdukModel>
) : RecyclerView.Adapter<ProdukAdapter.ProdukViewHolder>() {
    inner class ProdukViewHolder(val binding: ItemProdukBinding) : RecyclerView.ViewHolder(
        binding.root){
        fun binding(data: ProdukModel) {
            binding.tvTitleNews.text = data.title
            binding.tvSubtitleNews.text = data.subtitle
            binding.ivItemNews.setImageResource(data.image ?: 0)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProdukViewHolder
            = ProdukViewHolder(
        ItemProdukBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun getItemCount(): Int = dataProduk.size

    override fun onBindViewHolder(holder: ProdukViewHolder, position: Int) {
        holder.binding(dataProduk[position])
    }
}