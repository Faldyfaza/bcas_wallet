package com.example.bcas_wallet.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bcas_wallet.databinding.ItemPromoBinding
import com.example.bcas_wallet.model.PromoModel


class PromoAdapter(
    private val data: List<PromoModel>
) : RecyclerView.Adapter<PromoAdapter.PromoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PromoViewHolder
         = PromoViewHolder(
            ItemPromoBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: PromoViewHolder, position: Int) {
        holder.binding(data[position])
    }

    inner class PromoViewHolder(val binding: ItemPromoBinding) :
        RecyclerView.ViewHolder(binding.root){
        fun binding(data: PromoModel) {
            binding.tvTitleNews.text = data.title
            binding.tvSubtitleNews.text = data.subtitle
            binding.ivItemNews.setImageResource(data.image ?: 0)
        }
    }
}