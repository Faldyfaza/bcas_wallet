package com.example.bcas_wallet.view.register

import android.content.Intent
import android.os.Bundle
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity
import com.example.bcas_wallet.MainActivity
import com.example.bcas_wallet.databinding.ActivityRegisterBinding
import com.example.bcas_wallet.view.login.LoginActivity

class RegisterActivity : AppCompatActivity(){
    private lateinit var binding: ActivityRegisterBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnRegister.setOnClickListener{
            val inputName = binding.etNama.text.toString()
            val inputEmail = binding.etEmail.text.toString()
            val inputPassword = binding.etPassword.text.toString()
            navigateScreen(LoginActivity::class.java, inputName,inputEmail, inputPassword)
        }

    }

    private fun navigateScreen(screen: Class<*>, name: String,email: String, password: String) {
        val intent = Intent(applicationContext, screen)
        intent.putExtra(KEY_NAME, name)
        intent.putExtra(KEY_EMAIL, email)
        intent.putExtra(KEY_PASSWORD,password)

        startActivity(intent)
    }

    companion object {
        const val KEY_NAME = "name"
        const val KEY_EMAIL = "email"
        const val KEY_PASSWORD = "password"
    }
}