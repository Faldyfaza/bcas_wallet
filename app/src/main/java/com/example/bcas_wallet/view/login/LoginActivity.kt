package com.example.bcas_wallet.view.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity
import com.example.bcas_wallet.MainActivity
import com.example.bcas_wallet.databinding.ActivityLoginBinding
import com.example.bcas_wallet.view.register.RegisterActivity

class LoginActivity : AppCompatActivity(){
    private lateinit var binding: ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.btnLogin.setOnClickListener{
            val inputEmail = binding.etEmail.text.toString()
            val inputPassword = binding.etPassword.text.toString()
            navigateScreen(MainActivity::class.java, inputEmail, inputPassword)
        }
        binding.btnRegister.setOnClickListener{
            navigateRegister(RegisterActivity::class.java)
        }

    }

    private fun navigateScreen(screen: Class<*>, email: String, password: String) {
        val intent = Intent(applicationContext, screen)
        intent.putExtra(KEY_EMAIL, email)
        intent.putExtra(KEY_PASSWORD,password)

        startActivity(intent)
    }

    private fun navigateRegister(register:Class<*>){
        val intent = Intent(applicationContext, register)
        startActivity(intent)
    }

    companion object {
        const val KEY_EMAIL = "email"
        const val KEY_PASSWORD = "password"
    }
}