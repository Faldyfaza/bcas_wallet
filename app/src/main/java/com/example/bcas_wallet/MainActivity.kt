package com.example.bcas_wallet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.bcas_wallet.databinding.ActivityMainBinding
import com.example.bcas_wallet.view.HomeMainActivity
import com.example.bcas_wallet.view.login.LoginActivity
import com.example.bcas_wallet.view.login.LoginActivity.Companion.KEY_EMAIL
import com.example.bcas_wallet.view.login.LoginActivity.Companion.KEY_PASSWORD

class MainActivity : AppCompatActivity() {
    private  lateinit var binding : ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val email = intent.getStringExtra(KEY_EMAIL)
        val password = intent.getStringExtra(KEY_PASSWORD)
        binding.tvEmail.text=email
        binding.tvPassword.text=password

        binding.btnPromo.setOnClickListener{
            navigatePromo(HomeMainActivity::class.java)
        }

        binding.btnLogout.setOnClickListener{
            logout(LoginActivity::class.java)
        }
    }

    private fun navigatePromo(promo:Class<*>){
        startActivity(Intent(this, promo))
    }

    private fun logout(logout:Class<*>){
        startActivity(Intent(this, logout))
    }
}